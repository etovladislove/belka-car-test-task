<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:55
 */

namespace App\Enum;


class PlanTypeEnum
{
    const PER_KILOMETER = 1;
    const PER_MINUTE = 2;
    const TIME_RANGE = 3;
}