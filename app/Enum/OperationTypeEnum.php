<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:54
 */

namespace App\Enum;


class OperationTypeEnum
{
    const BOOKING = 1;
    const INSPECTION = 2;
    const TRIP = 3;
    const PARKING = 4;
    const BABY_SEAT = 5;
}