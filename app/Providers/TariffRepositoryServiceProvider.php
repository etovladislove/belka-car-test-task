<?php
namespace App\Providers;


use App\Repository\Impl\TariffRepositoryImpl;
use App\Repository\TariffRepository;
use Illuminate\Support\ServiceProvider;

class TariffRepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(TariffRepository::class, function () {
            return new TariffRepositoryImpl();
        });
    }
}