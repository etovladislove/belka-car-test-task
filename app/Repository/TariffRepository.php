<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:21
 */

namespace App\Repository;

use App\Model\Tariff;

/**
 * Interface TariffRepository
 * @package App\Repository
 */
interface TariffRepository
{

    /**
     * Find tariff by id
     *
     * @param int $id
     * @return Tariff|null
     */
    public function findById(int $id): ?Tariff;
}