<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:25
 */

namespace App\Repository\Impl;


use App\Model\Tariff;
use App\Repository\TariffRepository;
use Illuminate\Http\Request;

/**
 * Class TariffRepositoryImpl
 * @package App\Repository\Impl
 */
class TariffRepositoryImpl implements TariffRepository
{

    /**
     * Find tariff by id
     *
     * @param int $id
     * @return Tariff|null
     */
    public function findById(int $id): ?Tariff
    {
        return Tariff::with([
            'tariffOptions',
            'maximumDailyCost',
            'tariffOptions.option.operationType',
            'tariffOptions.option.planType'
        ])->find($id);
    }
}