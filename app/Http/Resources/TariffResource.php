<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TariffResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'maximum_daily_cost' => $this->maximumDailyCost->cost,
            'tariff_options' => TariffOptionResource::collection($this->tariffOptions)
        ];
    }
}
