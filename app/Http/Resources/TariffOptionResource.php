<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TariffOptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'cost' => $this->cost,
            'from' => $this->from,
            'to' => $this->to,
            'plus_to_maximum_daily_cost' => $this->option->plus_to_maximum_daily_cost,
            'operation_type' => $this->option->operationType->name,
            'plan_type' => $this->option->planType->name,
        ];
    }
}
