<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 19/03/2019
 * Time: 22:51
 */

namespace App\Http\Controllers;

use App\Http\Resources\TariffResource;
use App\Repository\TariffRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GetTariffController
 * @package App\Http\Controllers
 */
class GetTariffController extends Controller
{

    /** @var  TariffRepository */
    protected $tariffRepository;

    /** @var  Request */
    protected $request;

    protected $rules = [
        'id' => 'required|int|exists:tariffs'
    ];

    /**
     * GetTariffController constructor.
     * @param TariffRepository $tariffRepository
     * @param Request $request
     */
    public function __construct(TariffRepository $tariffRepository, Request $request)
    {
        $this->tariffRepository = $tariffRepository;
        $this->request = $request;
    }

    public function findById($id)
    {
        $validator = Validator::make(['id' => $id], $this->rules);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $tariffModel = $this->tariffRepository->findById($id);
        return new TariffResource($tariffModel);
    }

}