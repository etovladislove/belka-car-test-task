<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 19/03/2019
 * Time: 22:44
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Option
 * @package App\Model
 */
class Option extends Model
{

    public function planType()
    {
        return $this->belongsTo(PlanType::class);
    }

    public function operationType()
    {
        return $this->belongsTo(OperationType::class);
    }
}