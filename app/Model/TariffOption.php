<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:19
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * Class TariffOption
 * @package App\Model
 */
class TariffOption extends Model
{

    public function option()
    {
        return $this->belongsTo(Option::class);
    }
}