<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:20
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Tariff
 * @package App\Model
 */
class Tariff extends Model
{

    public function tariffOptions()
    {
        return $this->hasMany(TariffOption::class);
    }

    public function maximumDailyCost()
    {
        return $this->belongsTo(MaximumDailyCost::class);
    }
}