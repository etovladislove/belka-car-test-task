<?php
/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 19/03/2019
 * Time: 22:42
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Car
 * @package App\Model
 */
class Car extends Model
{

    public function status()
    {
        return $this->hasOne(CarStatus::class);
    }
}