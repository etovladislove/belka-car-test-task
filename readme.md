**How to start:**

1) Create .env file from .env.example
2)  `php artisan key:generate`
3)  `php artisan migrate`
4)  `php artisan db:seed`

**URL for get tariff [GET]:**
`http://localhost:port/tariff/{id}`

**After seeding - database has one tariff with ID 1**


