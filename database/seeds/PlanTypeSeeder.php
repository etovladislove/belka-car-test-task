<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:43
 */
class PlanTypeSeeder extends Seeder
{

    public function run()
    {
        DB::statement('TRUNCATE plan_types CASCADE');

        DB::table('plan_types')->insert([
            ['id' => '1', 'name' => 'per kilometer'],
            ['id' => '2', 'name' => 'per_minute'],
            ['id' => '3', 'name' => 'time_range']
        ]);
    }

}