<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:43
 */
class OperationTypeSeeder extends Seeder
{

    public function run()
    {
        DB::statement('TRUNCATE operation_types CASCADE');

        DB::table('operation_types')->insert([
            ['id' => '1', 'name' => 'booking'],
            ['id' => '2', 'name' => 'inspection'],
            ['id' => '3', 'name' => 'trip'],
            ['id' => '4', 'name' => 'parking'],
            ['id' => '5', 'name' => 'baby_seat']
        ]);
    }

}