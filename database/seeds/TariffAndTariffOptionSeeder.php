<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:43
 */
class TariffAndTariffOptionSeeder extends Seeder
{

    public function run()
    {
        DB::statement('TRUNCATE tariffs CASCADE');

        DB::table('tariffs')->insert([
            ['id' => '1', 'name' => 'Little squirrel', 'maximum_daily_cost_id' => 1],
            ['id' => '2', 'name' => 'Serious squirrel', 'maximum_daily_cost_id' => 2],
        ]);

        DB::table('tariff_options')->insert([
            [
                'id' => '1',
                'tariff_id' => 1,
                'option_id' => 1,
                'cost' => 0,
                'from' => 0,
                'to' => 20,
            ],
            [
                'id' => '2',
                'tariff_id' => 1,
                'option_id' => 1,
                'cost' => 5,
                'from' => 20,
                'to' => null
            ],
            [
                'id' => '3',
                'tariff_id' => 1,
                'option_id' => 2,
                'cost' => 0,
                'from' => 23 * 60 * 60,
                'to' => 7 * 60 * 60
            ],
            [
                'id' => '4',
                'tariff_id' => 1,
                'option_id' => 3,
                'cost' => 0,
                'from' => 0,
                'to' => 7,
            ],
            [
                'id' => '5',
                'tariff_id' => 1,
                'option_id' => 3,
                'cost' => 3,
                'from' => 7,
                'to' => null
            ],
            [
                'id' => '6',
                'tariff_id' => 1,
                'option_id' => 4,
                'cost' => 5,
                'from' => 0,
                'to' => null
            ],
            [
                'id' => '7',
                'tariff_id' => 1,
                'option_id' => 5,
                'cost' => 3,
                'from' => 70,
                'to' => null
            ],
            [
                'id' => '8',
                'tariff_id' => 1,
                'option_id' => 6,
                'cost' => 2,
                'from' => 0,
                'to' => null
            ],
            [
                'id' => '9',
                'tariff_id' => 1,
                'option_id' => 7,
                'cost' => 0,
                'from' => 23 * 60 * 60,
                'to' => 7 * 60 * 60
            ],
            [
                'id' => '10',
                'tariff_id' => 1,
                'option_id' => 7,
                'cost' => 2,
                'from' => 0,
                'to' => null
            ],
        ]);
    }

}