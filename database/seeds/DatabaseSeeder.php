<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlanTypeSeeder::class);
        $this->call(OperationTypeSeeder::class);
        $this->call(OptionSeeder::class);
        $this->call(MaximumDailyCostSeeder::class);
        $this->call(TariffAndTariffOptionSeeder::class);
    }
}
