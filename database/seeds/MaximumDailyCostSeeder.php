<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:43
 */
class MaximumDailyCostSeeder extends Seeder
{

    public function run()
    {
        DB::statement('TRUNCATE maximum_daily_costs CASCADE');

        DB::table('maximum_daily_costs')->insert([
            ['id' => '1', 'cost' => 2700],
            ['id' => '2', 'cost' => 5000]
        ]);
    }

}