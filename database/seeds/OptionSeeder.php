<?php

use App\Enum\OperationTypeEnum;
use App\Enum\PlanTypeEnum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: etovladislav
 * Date: 20/03/2019
 * Time: 11:43
 */
class OptionSeeder extends Seeder
{
    public function run()
    {
        DB::statement('TRUNCATE options CASCADE');

        DB::table('options')->insert([
            [
                'id' => '1',
                'operation_type_id' => OperationTypeEnum::BOOKING,
                'plan_type_id' => PlanTypeEnum::PER_MINUTE,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '2',
                'operation_type_id' => OperationTypeEnum::BOOKING,
                'plan_type_id' => PlanTypeEnum::TIME_RANGE,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '3',
                'operation_type_id' => OperationTypeEnum::INSPECTION,
                'plan_type_id' => PlanTypeEnum::PER_MINUTE,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '4',
                'operation_type_id' => OperationTypeEnum::TRIP,
                'plan_type_id' => PlanTypeEnum::PER_MINUTE,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '5',
                'operation_type_id' => OperationTypeEnum::TRIP,
                'plan_type_id' => PlanTypeEnum::PER_KILOMETER,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '6',
                'operation_type_id' => OperationTypeEnum::PARKING,
                'plan_type_id' => PlanTypeEnum::PER_MINUTE,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '7',
                'operation_type_id' => OperationTypeEnum::PARKING,
                'plan_type_id' => PlanTypeEnum::TIME_RANGE,
                'plus_to_maximum_daily_cost' => 0
            ],
            [
                'id' => '8',
                'operation_type_id' => OperationTypeEnum::BABY_SEAT,
                'plan_type_id' => PlanTypeEnum::PER_MINUTE,
                'plus_to_maximum_daily_cost' => 300
            ],
        ]);
    }
}