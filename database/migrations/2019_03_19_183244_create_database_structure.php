<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabaseStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('car_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("car_id")->unsigned();
            $table->foreign("car_id")->references("id")->on("cars")->onDelete("cascade");
            $table->string('status', 30)->comment('Car status: free, booked..');
        });

        // Book, inspection, trip
        Schema::create('operation_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Book, inspection, trip');
        });

        Schema::create('plan_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('mileage, per minute rate etc.');
        });

        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("operation_type_id")->unsigned();
            $table->foreign("operation_type_id")->references("id")->on("operation_types")->onDelete("cascade");
            $table->integer("plan_type_id")->unsigned();
            $table->foreign("plan_type_id")->references("id")->on("plan_types")->onDelete("cascade");
            $table->double('plus_to_maximum_daily_cost')->nullable();
        });
        Schema::create('maximum_daily_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cost');
        });
        Schema::create('tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("maximum_daily_cost_id")->unsigned();
            $table->foreign("maximum_daily_cost_id")->references("id")->on("maximum_daily_costs")->onDelete("cascade");
            $table->string('name');
        });
        Schema::create('tariff_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("tariff_id")->unsigned();
            $table->foreign("tariff_id")->references("id")->on("tariffs")->onDelete("cascade");
            $table->integer("option_id")->unsigned();
            $table->foreign("option_id")->references("id")->on("options")->onDelete("cascade");
            $table->double('cost')->nullable();
            $table->double('from')->nullable();
            $table->double('to')->nullable();
        });


        Schema::create('rents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->unsigned();
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->integer("car_id")->unsigned();
            $table->foreign("car_id")->references("id")->on("cars")->onDelete("cascade");
            $table->integer("tariff_id")->unsigned();
            $table->foreign("tariff_id")->references("id")->on("tariffs")->onDelete("cascade");
            $table->double('total_cost', 8, 1)->default(0);
            $table->string('status');
            $table->timestamps();
        });

        Schema::create('rent_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("rent_id")->unsigned();
            $table->foreign("rent_id")->references("id")->on("rents")->onDelete("cascade");
            $table->integer("option_id")->unsigned();
            $table->foreign("option_id")->references("id")->on("options")->onDelete("cascade");
            $table->double('duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariff_options');
        Schema::dropIfExists('tariffs');
        Schema::dropIfExists('maximum_daily_costs');
        Schema::dropIfExists('rent_options');
        Schema::dropIfExists('options');
        Schema::dropIfExists('plan_types');
        Schema::dropIfExists('operation_types');
        Schema::dropIfExists('rents');
        Schema::dropIfExists('car_statuses');
        Schema::dropIfExists('cars');
    }
}
